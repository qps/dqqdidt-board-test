#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to measure the noise properties of each of the three DQQDIDT
channels using the automated measurement framework.

General idea:
    - Short the input of the channel and record a buffer
    - Analyze the noise levels:
        - peak-peak noise
        - standard deviation
        - main frequency components

It uses the functions and classes of the automated HW testing framework:
    https://gitlab.cern.ch/qps/automated_hw_testing

@author: Severin Haas at CERN, March 2020
"""
# pylint: disable=C0103

import time
import numpy as np
import serial
import matplotlib.pyplot as plt
from lib.uqds import UQDS
import datahandling.signal_analysis as signal


####################### Test Parameters #######################################
dut_name = "3 Channel Board v1.0"
board_number = 1
path = 'C:/Users/qpsop/cernbox_shaas/Win_Sync/3channel_test/'
fft_image_filename = 'dqqdidt_board' + str(board_number) + '_fft_'
hist_image_filename = 'dqqdidt_board' + str(board_number) + '_hist_'


# Define the address for reading the buffer
address_start = 96
address_stop = 100
samples = 8000

# Board settings
clock = 40.0e+6
dqqdidt_lsb = 2**-24
dqqdidt_a_lsb = 2**-24 / 2.5 # channel A
adc_speed = 200e+3
adc_set_value = int(clock / adc_speed)
sample_rate = clock / adc_set_value
f_resolution = sample_rate / samples

channel_a_range = 20 # +-10 Volt
channel_range = 50 # +- 25 Volt
channel_a_lsb = channel_a_range / (2**20)
channel_lsb = channel_range / (2**20)

# FFT plot area
fft_ylim = [-155, -100]

# Define the COM port to use (see device manager setting on windows)
com = 'COM22'

# Multiplication factor to show e.g. uV instead of V
mult = int(1e+6)
if mult == 1:
    val = "V"
elif mult == int(1e+3):
    val = "mV"
elif mult == int(1e+6):
    val = "uV"


####################### Setup #################################################
# Create port for communication
ser = serial.Serial()

# Instanciate an UART communication object with UQDS class
board_obj = UQDS(ser, com)


####################### Load Parameters #######################################
board_obj.reset()
time.sleep(5)
print("Reset done")

# # Set ADC speed
# board_obj.write_register(12, [0x00, 0x00, 0x00, adc_set_value])
# board_obj.write_register(12, [0x00, 0x00, 0x00, 0x75])
# time.sleep(10e-3)

# Set readout config
board_obj.write_register(10, [0x64, 0x00, 0x00, 0x00])
time.sleep(10e-3)

for i in range(19, 34):
    board_obj.write_register(i, [0x00, 0x00, 0x00, 0x00])
    time.sleep(10e-3)

for i in range(128, 139):
    board_obj.write_register(i, [0x00, 0x00, 0x00, 0x00])
    time.sleep(10e-3)

time.sleep(1)
print("New config done")


####################### Aquiring data #########################################
data_ch1 = np.array([])
data_ch2 = np.array([])
data_ch3 = np.array([])

board_obj.write_register(0x00, [0x13, 0x00, 0x00, 0x00])
print("Board triggered")

time.sleep(3)
for i in range(0, 200):
    data = board_obj.read_register(i, 5)

    if i == 7:
        print("System status: " + str(data))

time.sleep(3)
for i in range(0, 200):
    data = board_obj.read_register(i, 5)

    if i == 7:
        print("System status: " + str(data))

    if i == 12:
        set_sps = data

buffer = board_obj.read_buffer(samples, address_start, address_stop)
# buffer = board_obj.trig_and_read_buffer(samples, adress_start, adress_stop, system_flag=0xea)

# Close connection
ser.close()

data_ch1 = np.multiply(buffer[:, 0], dqqdidt_a_lsb)
data_ch2 = np.multiply(buffer[:, 1], dqqdidt_lsb)
data_ch3 = np.multiply(buffer[:, 2], dqqdidt_lsb)


####################### Noise analysis ########################################
print("Used n = " + str(len(data_ch1)) + " samples for analysis.")
set_adc_value = board_obj.convert_bytes_array(set_sps, 5)
sample_rate = (clock / int(set_adc_value[3]))
print("fs = %.3f" % (sample_rate / 1e+3) + " kHz")

# Mean value
ch1_mean = signal.calculate_mean(data_ch1)
ch2_mean = signal.calculate_mean(data_ch2)
ch3_mean = signal.calculate_mean(data_ch3)

# Standard deviation
ch1_std_dev = signal.calculate_std_dev(data_ch1)
ch2_std_dev = signal.calculate_std_dev(data_ch2)
ch3_std_dev = signal.calculate_std_dev(data_ch3)
ch1_std_dev_lsb = ch1_std_dev / channel_a_lsb
ch2_std_dev_lsb = ch2_std_dev / channel_lsb
ch3_std_dev_lsb = ch3_std_dev / channel_lsb

print("Ch1: %.3f" % np.multiply(ch1_mean, mult) + " " + val + \
      " +- %.3f" % np.multiply(ch1_std_dev, mult)+ \
      " (%.2f" % ch1_std_dev_lsb + " LSB)")
print("Ch2: %.3f" % np.multiply(ch2_mean, mult) + " " + val + \
      " +- %.3f" % np.multiply(ch2_std_dev, mult) + \
      " (%.2f" % ch2_std_dev_lsb + " LSB)")
print("Ch3: %.3f" % np.multiply(ch3_mean, mult) + " " + val + \
      " +- %.3f" % np.multiply(ch3_std_dev, mult)+ \
      " (%.2f" % ch3_std_dev_lsb + " LSB)\n")


# Peak-Peak noise
ch1_pp = signal.calculate_pp_noise(data_ch1)
ch2_pp = signal.calculate_pp_noise(data_ch2)
ch3_pp = signal.calculate_pp_noise(data_ch3)
ch1_pp_lsb = ch1_pp / channel_a_lsb
ch2_pp_lsb = ch2_pp / channel_lsb
ch3_pp_lsb = ch3_pp / channel_lsb
print("Ch1 pp noise: %.3f" % np.multiply(ch1_pp, mult) + val + ", %.2f" % ch1_pp_lsb + " LSB")
print("Ch2 pp noise: %.3f" % np.multiply(ch2_pp, mult) + val + ", %.2f" % ch2_pp_lsb + " LSB")
print("Ch3 pp noise: %.3f" % np.multiply(ch3_pp, mult) + val + ", %.2f" % ch3_pp_lsb + " LSB")

# Plot a histogram
signal.plot_hist(data_ch1 * mult, 20, 'Ch1', 'Voltage', val)
plt.tight_layout()
plt.savefig(path + hist_image_filename + 'ch1.png')

signal.plot_hist(data_ch2 * mult, 20, 'Ch2', 'Voltage', val)
plt.tight_layout()
plt.savefig(path + hist_image_filename + 'ch2.png')

signal.plot_hist(data_ch3 * mult, 20, 'Ch3', 'Voltage', val)
plt.tight_layout()
plt.savefig(path + hist_image_filename + 'ch3.png')



####################### FFT analysis ##########################################
# Applying hamming window
num_fft = len(data_ch1)
window = np.hanning(num_fft)

# FFT
[ch1_fft_x, ch1_fft_y] = signal.calculate_fft(data_ch1 * window, sample_rate)
[ch2_fft_x, ch2_fft_y] = signal.calculate_fft(data_ch2 * window, sample_rate)
[ch3_fft_x, ch3_fft_y] = signal.calculate_fft(data_ch3 * window, sample_rate)

y_fft_abs1 = np.abs(ch1_fft_y[0:num_fft//2])
y_fft_scaled1 = 2.0 / num_fft * y_fft_abs1 * 2 * np.sqrt(2) * (np.sqrt(2))
y_fft_dbfs1 = 20 * np.log10(y_fft_scaled1 / 10)

y_fft_abs2 = np.abs(ch2_fft_y[0:num_fft//2])
y_fft_scaled2 = 2.0 / num_fft * y_fft_abs2 * 2 * np.sqrt(2) * (np.sqrt(2))
y_fft_dbfs2 = 20 * np.log10(y_fft_scaled2 / 10)

y_fft_abs3 = np.abs(ch3_fft_y[0:num_fft//2])
y_fft_scaled3 = 2.0 / num_fft * y_fft_abs3 * 2 * np.sqrt(2) * (np.sqrt(2))
y_fft_dbfs3 = 20 * np.log10(y_fft_scaled3 / 10)


# Plot an overview of all three FFTs with automatic scaling
plt.figure(figsize=[8, 4])
plt.subplot(131)
plt.plot(ch1_fft_x / 1e+3, y_fft_dbfs1, label="Ch1")
plt.legend()
plt.xlabel("f (kHz)")
plt.ylabel("Signal (dBFs)")
plt.grid(True)

plt.subplot(132)
plt.plot(ch2_fft_x / 1e+3, y_fft_dbfs2, label="Ch2")
plt.legend()
plt.xlabel("f (kHz)")
plt.ylabel("Signal (dBFs)")
plt.grid(True)

plt.subplot(133)
plt.plot(ch3_fft_x / 1e+3, y_fft_dbfs3, label="Ch3")
plt.legend()
plt.xlabel("f (kHz)")
plt.ylabel("Signal (dBFs)")
plt.grid(True)


# Plot each FFT separtely in a bigger window
plt.figure(figsize=[8, 4])
plt.plot(ch1_fft_x / 1e+3, y_fft_dbfs1, label="Ch1")
plt.legend()
plt.xlabel("f (kHz)")
plt.ylabel("Signal (dBFs)")
plt.xlim([0, ch1_fft_x[len(ch1_fft_x) - 1] / 1e+3])
plt.ylim(fft_ylim)
plt.grid(True)
plt.tight_layout()
plt.savefig(path + fft_image_filename + 'ch1.png')

plt.figure(figsize=[8, 4])
plt.plot(ch2_fft_x / 1e+3, y_fft_dbfs2, label="Ch2")
plt.legend()
plt.xlabel("f (kHz)")
plt.ylabel("Signal (dBFs)")
plt.xlim([0, ch2_fft_x[len(ch2_fft_x) - 1] / 1e+3])
plt.ylim(fft_ylim)
plt.grid(True)
plt.tight_layout()
plt.savefig(path + fft_image_filename + 'ch2.png')

plt.figure(figsize=[8, 4])
plt.plot(ch3_fft_x / 1e+3, y_fft_dbfs3, label="Ch3")
plt.legend()
plt.xlabel("f (kHz)")
plt.ylabel("Signal (dBFs)")
plt.xlim([0, ch3_fft_x[len(ch3_fft_x) - 1] / 1e+3])
plt.ylim(fft_ylim)
plt.grid(True)
plt.tight_layout()
plt.savefig(path + fft_image_filename + 'ch3.png')
